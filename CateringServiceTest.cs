﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Text;
using System.IO;
using ExternalServices.Dominio;
using System.Web.Script.Serialization;

namespace ExternalServicesTest
{
    [TestClass]
    public class CateringServiceTest
    {
        [TestMethod]
        public void EliminarReservaTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:52707/CateringServiceRest.svc/ServiciosCatering/200");
                req.Method = "DELETE";
                req.ContentLength = data.Length;
                req.ContentType = "application/json";
                var reqStream = req.GetRequestStream();
                reqStream.Write(data, 0, data.Length);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string servicioJson = reader.ReadToEnd();
                Assert.AreEqual("true", servicioJson);
            }
            catch (WebException e)
            {
            }
        }
        [TestMethod]
        public void SolicitarReservaTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            string postdata = "{\"NombreServicio\":\"BEBIDAS\",\"DescripcionServicio\":\"CAFE\",\"Cantidad\":\"40\",\"FechaPedido\":\"20160912\",\"CodigoReserva\":\"000\" } "; //JSON
            byte[] data = Encoding.UTF8.GetBytes(postdata);
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:52707/CateringServiceRest.svc/ServiciosCatering");
                req.Method = "POST";
                req.ContentLength = data.Length;
                req.ContentType = "application/json";
                var reqStream = req.GetRequestStream();
                reqStream.Write(data, 0, data.Length);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string servicioJson = reader.ReadToEnd();
                Assert.AreNotEqual(servicioJson, null);
            }
            catch (WebException e)
            {
            }
        }
    }
}
