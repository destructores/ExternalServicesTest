﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ExternalServices.Dominio
{

    public class ReservaCatering
    {
 	
        public string NombreServicio { get; set; }

  
        public string CodigoReserva { get; set; }
        
   
        public string DescripcionServicio { get; set; }

     
        public int Cantidad { get; set; }

       
        public string FechaPedido { get; set; }
    }
}